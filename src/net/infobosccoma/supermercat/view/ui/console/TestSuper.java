/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.supermercat.view.ui.console;
import java.time.LocalDate;
import net.infobosccoma.supermercat.model.entities.CarroCompra;
import net.infobosccoma.supermercat.model.entities.Aliment;
import net.infobosccoma.supermercat.model.entities.Electronica;
import net.infobosccoma.supermercat.model.entities.Producte;


public class TestSuper {
    public static void main(String[] args) {
       // int opcio = 0;
        CarroCompra carro = new CarroCompra(100);
        
        Producte a1 = new Aliment("Aliment1", "111", 1, LocalDate.of(1947,1,1));//caducat
        carro.afegirProducte(a1);
        Producte a2 = new Aliment("Aliment2", "122", 2, LocalDate.now());//no caducat(data actual)
        carro.afegirProducte(a2);
        Producte e1 = new Electronica("Electronica1", "211", 10, 1825);//5 anys
        carro.afegirProducte(e1);
        Producte e2 = new Electronica("Electronica2", "222", 20, 1095);//3 anys
        carro.afegirProducte(e2);
        Producte e3 = new Electronica("Electronica3", "233", 30, 365);//1 any
        carro.afegirProducte(e3);
        carro.mostrarCaducats();
        // interficie nova, al costat de producte en la UML, 
        //amb un métode que comprovi si la data del producte es valida, accedit per aliment i electronica.
        
    }
}